#ifndef PPM_HPP
#define PPM_HPP

#include "processa.hpp"
#include <string>

class PPM : public Processa{
public:

  PPM(char endereco[100]);

  char decifraRespostaPPM(int cont,int inicio,std::string nomeArquivoSaidaPPM);

};
#endif
