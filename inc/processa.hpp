#ifndef PROCESSA_HPP
#define PROCESSA_HPP

class Processa{

protected:

  char endereco[100];
  int i;
  int posicao;
  int largura;
  int comprimento;
  int camada;
  char extraiPixel;
  char resposta;


public:

  Processa();
  void setPosicao(int posicao);
  void setLargura(int largura);
  void setComprimento(int comprimento);
  void setCamada(int camada);

};
#endif
