#ifndef PGM_HPP
#define PGM_HPP

#include "processa.hpp"

class PGM : public Processa{
public:

  PGM(char endereco[100]);

  char decifraRespostaPGM(int cont,int inicio);

};
#endif
