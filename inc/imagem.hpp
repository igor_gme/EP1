#ifndef IMAGEM_HPP
#define IMAGEM_HPP

class Imagem{

public:
  int contador;
  int aux;
  int pixel;

protected:
  char endereco[100];
  char informacao[];
  int i;
  char extraiPixel;

  char numeroMagico[2];
  int posicao;
  int largura;
  int comprimento;
  int camada;
  int inicio;

 
public:
  
  Imagem();
  
  Imagem(char endereco[100]);

  
    char getNumeroMagico();
    int getPosicao();
    int getLargura();
    int getComprimento();
    int getCamada();
    int getContador();

};
#endif
