#include <iostream>
#include <fstream>
#include <string>
#include "PPM.hpp"

using namespace std;

ifstream arquivoPPM;

PPM::PPM(char endereco[100]){
  arquivoPPM.open(endereco,fstream::in);

}

char PPM::decifraRespostaPPM(int inicio,int filtro,string nomeArquivoSaidaPPM){
  int i,j,k;
  char c='0';
  ofstream arquivoRespostaPPM;
  arquivoRespostaPPM.open(nomeArquivoSaidaPPM);
  arquivoPPM.seekg(inicio,ios_base::beg);

  arquivoRespostaPPM<<"P6\n";

  arquivoRespostaPPM<<largura<<" ";
  arquivoRespostaPPM<<comprimento<<"\n";
  arquivoRespostaPPM<<camada<<"\n";

  for(i=0;i<largura*3;i++){
    for(j=0;j<comprimento;j++){
      for(k=0;k<3;k++){

        if(filtro==1){
          switch (k){
            case 0:
            arquivoPPM.get(resposta);
            arquivoRespostaPPM<<resposta;
            break;
            default :
            arquivoPPM.get(resposta);
            resposta=0x0;
            arquivoRespostaPPM<<resposta;
            break;
          }
        }


        if(filtro==2){
          switch (k){
            case 1:
            arquivoPPM.get(resposta);
            arquivoRespostaPPM<<resposta;
            break;
            default :
            arquivoPPM.get(resposta);
            resposta=0x0;
            arquivoRespostaPPM<<resposta;
            break;
          }
        }


        if(filtro==3){
          switch (k){
            case 2:
            arquivoPPM.get(resposta);
            arquivoRespostaPPM<<resposta;
            break;
            default :
            arquivoPPM.get(resposta);
            resposta=0x0;
            arquivoRespostaPPM<<resposta;
            break;
          }
        }

      }
    }
  }
  return c;
}
