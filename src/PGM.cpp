#include <iostream>
#include <fstream>
#include "PGM.hpp"

using namespace std;

ifstream arquivoPGM;

PGM::PGM(char endereco[100]){
  arquivoPGM.open(endereco,fstream::in);

}

char PGM::decifraRespostaPGM(int cont ,int inicio){

  char byteArquivo;
  int bitExtraido;
  int n=posicao+inicio+(cont*8);
  arquivoPGM.seekg(n,ios_base::beg);

    i=0;
    extraiPixel = 0x00;

    while(i<8){
      arquivoPGM.get(byteArquivo);
      bitExtraido=(byteArquivo & 0x01);
      extraiPixel = (extraiPixel | (bitExtraido));

      if(i<7){
        extraiPixel = extraiPixel << 1;
      }
      else{
        break;
      }
      i++;
    }

    return extraiPixel;
}
