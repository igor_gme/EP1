#include <iostream>
#include <fstream>
#include <cstring>
#include <unistd.h>

#include "imagem.hpp"
#include "PGM.hpp"
#include "PPM.hpp"

using namespace std;

int main(){

  int menu;

      cout<<"------------------------------ Bem Vindo ao Menu! ------------------------------\n";
      cout<<endl;

      cout<<"\t1- Abrir imagem\n"<< "\t2- Sair!"<<endl;
      cout<<"Digite a opção desejada: ";
      cin>>menu;
      Imagem * imagem_1;

    switch (menu) {
      case 1:
          while(1){
              char caminhoImagem[100];
              char numeroMagico[10]="";
              int posicao,largura,comprimento,camada,inicio,filtro;

              cout<<"Digite o caminho da imagem (Ex.: doc/lena.pgm ou doc/segredo.ppm): ";
              cin>>caminhoImagem;

              imagem_1=new Imagem(caminhoImagem);

              for(int i=0;i<2;i++){
                numeroMagico[i]=imagem_1->getNumeroMagico();
              }

              if(!strcmp(numeroMagico,"P5")){
                PGM * decifraPGM;
                decifraPGM = new PGM(caminhoImagem);

                posicao=imagem_1->getPosicao();
                largura=imagem_1->getLargura();
                comprimento=imagem_1->getComprimento();
                camada=imagem_1->getCamada();
                inicio=imagem_1->getContador();

                decifraPGM->setPosicao(posicao);
                decifraPGM->setLargura(largura);
                decifraPGM->setComprimento(comprimento);
                decifraPGM->setCamada(camada);

                string nomeArquivoSaidaPGM;

                cout<<"\n---------------------- Opção para revelar o segredo .PGM -----------------------\n";
                cout<<endl;
                cout<<"\nDigite o nome que deseja para arquivo saída (Ex.: resposta.txt): ";
                cin>>nomeArquivoSaidaPGM;

                ofstream arquivoResposta;
                arquivoResposta.open(nomeArquivoSaidaPGM);

                char resposta;
                int i=0;

                resposta=decifraPGM->decifraRespostaPGM(i,inicio);
                arquivoResposta<<resposta;
                i++;
                do{
                  resposta=decifraPGM->decifraRespostaPGM(i,inicio);
                  arquivoResposta<<resposta;
                  i++;
                }while(resposta!='#');
                system("clear");
                cout<<"A mensagem oculta se encontra no arquivo "<<nomeArquivoSaidaPGM<<" na pasta raiz."<<endl;
                cout<<"\nAplicação finalizada"<<endl;
                arquivoResposta.close();

                delete(decifraPGM);
                break;
            }

              else{
                if(!strcmp(numeroMagico,"P6")){

                  PPM * decifraPPM;
                  decifraPPM = new PPM(caminhoImagem);
                  cout<<"\n---------------------- Opção para aplicar o filtro .PPM ------------------------\n";
                  cout<<endl;
                  cout<<"Selecione um dos filtros disponíveis:"<<endl;
                  cout<<"\t1 - Filtro RED -\n"<<"\t2 - Filtro GREEN -\n"<<"\t3 - Filtro BLUE -\n"<<endl;
                  cout<<"Digite o valor de um filtro: ";
                  cin>>filtro;

                  while(1){
                    if((filtro==1) || (filtro==2) || (filtro==3)){
                      break;
                    }
                    else{
                      cout<<"Filtro não cadastrado, informe um filtro listado: ";
                      cin>>filtro;
                    }
                  }

                  largura=imagem_1->getLargura();
                  comprimento=imagem_1->getComprimento();
                  camada=imagem_1->getCamada();
                  inicio=imagem_1->getContador();

                  decifraPPM->setLargura(largura);
                  decifraPPM->setComprimento(comprimento);
                  decifraPPM->setCamada(camada);

                  string nomeArquivoSaidaPPM;

                  cout<<"Digite o nome que deseja para arquivo saída (Ex.: resposta.ppm): ";
                  cin>>nomeArquivoSaidaPPM;

                  char sucesso;
                  sucesso=decifraPPM->decifraRespostaPPM(inicio,filtro,nomeArquivoSaidaPPM);
                  if(sucesso=='0'){
                    system("clear");
                    cout<<"A imagem com o filtro aplicado se encontra no arquivo "<< nomeArquivoSaidaPPM<<"."<<endl;
                    cout<<"\nAplicação finalizada"<<endl;
                    break;
                  }
                  else{
                    cout<<"Ocorreu um erro no momento de decifrar"<<endl;
                    break;
                  }
                  delete(decifraPPM);
              }

              else{
                cout<<"Formato não suportado pela aplicação."<<endl;
                break;
              }
            }
        }
      break;

      case 2:
            system("clear");
            cout<<"\nTerminando a aplicação"<<endl;
            exit(0);
            delete(imagem_1);
      break;

      default:
            cout<<"\nOpção não Cadastrada" <<endl;
      break;
      cout<<endl;
    }
};
