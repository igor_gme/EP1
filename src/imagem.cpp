#include <iostream>
#include <cstring>
#include <fstream>
#include <string>
#include <stdlib.h>
#include "imagem.hpp"

using namespace std;

ifstream arquivo;

Imagem::Imagem(){
}


Imagem::Imagem(char endereco[100]){

   pixel=0;
   arquivo.open(endereco,fstream::in);

   if(!arquivo.is_open()){
      cout<<"Erro! Arquivo não encontrado.\n";
      cout<<endl;
      exit(0);
   }
}

char Imagem::getNumeroMagico(){
  contador=0;

  do{
    arquivo.get(informacao[contador]);
    numeroMagico[contador]=informacao[contador];
    return numeroMagico[contador];

    contador++;
  }while(informacao[contador-1]!='\n');
  pixel++;
  pixel+=contador;
}

int Imagem::getPosicao(){
  contador=0;
  string informacaoProvisoria;

  do{
    arquivo.get(informacao[contador]);

      if(informacao[contador]=='#'){
        aux=contador;
        arquivo.seekg(aux,ios_base::cur);

        do{
          informacao[contador]=arquivo.get();
          informacaoProvisoria+=informacao[contador];
          contador++;
        }while(informacao[contador-1]!=' ');
          pixel++;
          break;
      }
      else{
        contador++;
      }
  }while(1);

  pixel++;
  pixel+=contador;

  posicao = stoi (informacaoProvisoria);
  return posicao;
}

int Imagem::getLargura(){
  contador=0;
  string informacaoProvisoria;
  char auxiliar;

  arquivo.seekg(contador,ios_base::cur);

  do{
    arquivo.get(informacao[contador]);
    auxiliar=arquivo.peek();

    if(isdigit(informacao[contador])){
      if(isdigit(auxiliar)){
        informacaoProvisoria+=informacao[contador];
        contador++;
      }
      else{
        informacaoProvisoria+=informacao[contador];
        contador++;
        break;
      }
    }
    else{
      contador++;
    }
  }while(1);
    pixel++;
    pixel+=contador;

  largura = stoi (informacaoProvisoria);
  return largura;
}

int Imagem::getComprimento(){
  contador=0;
  string informacaoProvisoria;
  char auxiliar;

  arquivo.seekg(contador,ios_base::cur);

  do{
    arquivo.get(informacao[contador]);
    auxiliar=arquivo.peek();

    if(isdigit(informacao[contador])){
      if(isdigit(auxiliar)){
        informacaoProvisoria+=informacao[contador];
        pixel++;
      }
      else{
        informacaoProvisoria+=informacao[contador];
        pixel++;
        break;
      }
    }
    else{
      contador++;
    }
  }while(1);

    pixel++;
    pixel+=contador;

    comprimento = stoi (informacaoProvisoria);
    return comprimento;
}

int Imagem::getCamada(){

  contador=0;
  string informacaoProvisoria;
  char auxiliar;

  arquivo.seekg(contador,ios_base::cur);

  do{
    arquivo.get(informacao[contador]);
    auxiliar=arquivo.peek();

    if(isdigit(informacao[contador])){
      if(isdigit(auxiliar)){
        informacaoProvisoria+=informacao[contador];
        contador++;
      }
      else{
        informacaoProvisoria+=informacao[contador];
        pixel++;
        break;
      }
    }
    else{
      contador++;
    }
  }while(1);

  pixel++;
  pixel+=contador;

  camada = stoi (informacaoProvisoria);
  return camada;
}

int Imagem::getContador (){
  return pixel;
}
