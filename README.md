		EP1 - Orientação a Objetos
		Professor: Renato Sampaio Coral
		Aluno: Igor Gabriel M. Evangelista
		Matricula: 15/0037074
		Turma: B

	>>>Esteganografia em imagens, onde o programa decifra mensgens escondidas em imagens (.pgm) e aplica filtro RGB em imagens (.ppm).<<<

Localização das pastas:

obj/ (Objetos): Armazena todos os objetos criados pelas classes.

inc/ (Headers): Armazena todos os headers da classe.

src/ (Arquivos de Execução): Armazena todos os com instruções e com a main.cpp.

doc/ (Documentos): Armazena as imagens (.ppm/.pgm) que serão usadas no programa.

bin/ (Binario): Local onde fica o executavel.

/ (Pasta raíz): Armazena o README.md e o makefile para a compilação, além dos arquivos de saida, sendo eles, resposta da mensagem escondida(.txt) e o filtro aplicado na imagem(.ppm).



Instruções para execução do Cliente do programa:

	1. Assegure-se de ter instalado o programa GNU make (no terminal de execução).
	
	2. Para uma execução sem problemas sempre faça a limpeza dos Binários e dos Objetos.
		-Para fazer a limpeza dos binários e objetos digite em seu terminal:
		
					 make clean.

	3. Após feito a limpeza o seu programa está pronto para ser compilado:
		-Para compilar o programa digite em seu terminal:
			
					 make
	
	4. Após feito a limpeza e compilação o seu programa está pronto para ser executado:
		-Para executar o programa digite em seu terminal:
					
					 make run


